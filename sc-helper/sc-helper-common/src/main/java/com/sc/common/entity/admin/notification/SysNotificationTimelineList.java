package com.sc.common.entity.admin.notification;


/**
 * @author: wust
 * @date: 2020-04-09 13:26:08
 * @description:
 */
public class SysNotificationTimelineList extends SysNotificationTimeline {
    private String typeLabel;

    public String getTypeLabel() {
        return typeLabel;
    }

    public void setTypeLabel(String typeLabel) {
        this.typeLabel = typeLabel;
    }
}