package com.sc.common.interceptors.context;

import com.sc.common.annotations.AppApi;
import com.sc.common.annotations.FeignApi;
import com.sc.common.annotations.OpenApi;
import com.sc.common.annotations.WebApi;
import com.sc.common.context.DefaultBusinessContext;
import com.sc.common.enums.ApplicationEnum;
import com.sc.common.exception.BusinessException;
import com.sc.common.util.WebUtil;
import org.springframework.web.method.HandlerMethod;

/**
 * @author ：wust
 * @date ：Created in 2019/8/8 10:04
 * @description：
 * @version:
 */
public class StrategyContext {
    private StrategyContext(){}
    private static final StrategyContext instance = new StrategyContext();
    public static StrategyContext getInstance(){
        return instance;
    }

    public void setDefaultBusinessContext(HandlerMethod handlerMethod) {
        FeignApi feignApiAnnotation = handlerMethod.getBeanType().getAnnotation(FeignApi.class);
        OpenApi openApiAnnotation = handlerMethod.getBeanType().getAnnotation(OpenApi.class);
        AppApi appApiAnnotation = handlerMethod.getBeanType().getAnnotation(AppApi.class);
        WebApi webApiAnnotation = handlerMethod.getBeanType().getAnnotation(WebApi.class);

        String reqUrl = WebUtil.getRequest().getRequestURI();

        if (reqUrl.contains("/login")
                || reqUrl.contains("/logout")
                || reqUrl.contains("/getAppLoginQRCode")) {
            DefaultBusinessContext.getContext().setDataSourceId(ApplicationEnum.DEFAULT.name());
        }else{
            if(feignApiAnnotation != null){ // 内部api请求
                IStrategy feignStrategy = new FeignStrategy();
                feignStrategy.setDefaultBusinessContext();
            }else if(openApiAnnotation != null){ // 开放api请求
                IStrategy openApiStrategy = new OpenApiStrategy();
                openApiStrategy.setDefaultBusinessContext();
            }else if(appApiAnnotation != null){
                IStrategy appStrategy = new AppApiStrategy();
                appStrategy.setDefaultBusinessContext();
            } else if(webApiAnnotation != null){// web前端请求
                IStrategy webStrategy = new WebStrategy();
                webStrategy.setDefaultBusinessContext();
            }else{ // 其他请求
                throw new BusinessException("非法的请求：无法识别的接口类型！");
            }
        }
    }
}
