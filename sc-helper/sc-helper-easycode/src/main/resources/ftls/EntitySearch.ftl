package ${EntityPackageName};

import ${BasePackageName}.common.dto.PageDto;
import lombok.*;
import io.swagger.annotations.ApiModel;

/**
 * @author: ${Author}
 * @date: ${DateTime}
 * @description:
 */
 @ApiModel(description = "查询对象-${ClassName}Search")
 @NoArgsConstructor
 @AllArgsConstructor
 @ToString
 @Data
public class ${ClassName}Search extends ${ClassName} {
    private PageDto pageDto;
}