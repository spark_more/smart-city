package com.sc.order.core.service.impl;

import com.sc.order.core.dao.OrderMapper;
import com.sc.order.core.service.OrderService;
import com.sc.order.entity.order.Order;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import com.sc.common.dto.WebResponseDto;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

/**
 * @author: wust
 * @date: 2020-07-15 14:14:39
 * @description:
 */
@Service("orderServiceImpl")
public class OrderServiceImpl extends BaseServiceImpl<Order> implements OrderService{
    @Autowired
    private OrderMapper orderMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return orderMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
