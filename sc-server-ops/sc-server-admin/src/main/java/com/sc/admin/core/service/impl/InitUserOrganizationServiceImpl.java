/**
 * Created by wust on 2020-04-02 14:39:38
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.service.impl;

import com.sc.admin.core.service.SysUserOrganizationService;
import com.sc.common.service.InitializtionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Service;

/**
 * @author: wust
 * @date: Created in 2020-04-02 14:39:38
 * @description:
 *
 */
@Order(3)
@Service
public class InitUserOrganizationServiceImpl implements InitializtionService {

    @Autowired
    private SysUserOrganizationService sysUserOrganizationServiceImpl;

    @Override
    public void init() {
        sysUserOrganizationServiceImpl.init();
    }
}
