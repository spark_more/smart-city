/**
 * Created by wust on 2019-12-13 15:36:59
 * Copyright © 2019 wust. All rights reserved.
 */
package com.sc.admin.core.api.open;

import com.sc.admin.core.service.SysDistributedFileService;
import com.sc.common.annotations.OpenApi;
import com.sc.common.annotations.OperationLog;
import com.sc.common.entity.admin.distributedfile.SysDistributedFile;
import com.sc.common.enums.OperationLogEnum;
import com.sc.common.service.MinioStorageApiService;
import com.sc.common.service.MinioStorageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author: wust
 * @date: Created in 2019-12-13 15:36:59
 * @description: 开放api下载接口
 *
 */
@Api(tags = {"开放接口~下载文件"})
@OpenApi
@RequestMapping("/api/open/v1/DownloadOpenApi")
@RestController
public class DownloadOpenApi {
    @Autowired
    private SysDistributedFileService sysDistributedFileServiceImpl;

    @Autowired
    private MinioStorageApiService minioStorageApiService;

    @Autowired
    private MinioStorageService minioStorageServiceImpl;


    @ApiOperation(value = "下载文件", httpMethod = "GET")
    @ApiImplicitParams({
            @ApiImplicitParam(name="fileId",value="文件ID",required=true,paramType="query")
    })
    @OperationLog(moduleName= OperationLogEnum.MODULE_COMMON,businessName="下载文件",operationType= OperationLogEnum.Download)
    @RequestMapping(value = "",method = RequestMethod.GET,produces ="application/json;charset=utf-8")
    public ResponseEntity<byte[]> downloadFile(@RequestParam String fileId){
        HttpHeaders headers = new HttpHeaders();

        SysDistributedFile distributedFile = sysDistributedFileServiceImpl.selectByPrimaryKey(fileId);
        if(distributedFile == null){
            headers.setContentDispositionFormData("attachment", "error"); //下载后显示的名字
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            return new ResponseEntity(null, headers, HttpStatus.CREATED);  //向浏览器发送数据
        }
        InputStream inputStream = minioStorageApiService.download(Long.parseLong(fileId),minioStorageServiceImpl);
        try {
            byte[] bytes = IOUtils.toByteArray(inputStream);
            String fileName = new String((distributedFile.getFileName()).getBytes("UTF-8"),"iso-8859-1");
            headers.setContentDispositionFormData("attachment", fileName); //下载后显示的名字
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            return new ResponseEntity(bytes, headers, HttpStatus.CREATED);  // 向浏览器发送数据
        } catch (IOException e) {
            headers.setContentDispositionFormData("attachment", "error"); //下载后显示的名字
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);
            return new ResponseEntity(null, headers, HttpStatus.CREATED);  //向浏览器发送数据
        }
    }
}
