package com.sc.admin.core.service;

import com.alibaba.fastjson.JSONObject;
import com.sc.common.dto.WebResponseDto;
import com.sc.common.entity.admin.organization.SysOrganization;
import com.sc.common.entity.admin.organization.SysOrganizationSearch;
import com.sc.common.service.BaseService;
import java.util.Map;

/**
 * Created by wust on 2019/6/3.
 */
public interface SysOrganizationService extends BaseService<SysOrganization> {
    void initDefaultOrganization();


    /**
     * 构建组织架构树
     * 根据当前登录用户类型，获取其对应的组织架构
     * @param search
     * @return
     */
    WebResponseDto buildLeftTree(SysOrganizationSearch search);

    /**
     * 为角色设置功能权限
     * @param jsonObject
     * @return
     */
    WebResponseDto setFunctionPermissions(JSONObject jsonObject);


    /**
     * 创建公司
     * @param map
     * @return
     */
    WebResponseDto createCompany(Map map);


    /**
     * 创建项目
     * @param map
     * @return
     */
    WebResponseDto createProject(Map map);


    /**
     * 创建部门
     * @param map
     * @return
     */
    WebResponseDto createDepartment(Map map);


    /**
     * 创建岗位/角色
     * @param map
     * @return
     */
    WebResponseDto createRole(Map map);


    /**
     * 添加员工
     * @param map
     * @return
     */
    WebResponseDto addUser(Map map);
}
