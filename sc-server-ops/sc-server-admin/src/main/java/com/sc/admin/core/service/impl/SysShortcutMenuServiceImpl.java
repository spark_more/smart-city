package com.sc.admin.core.service.impl;

import com.sc.admin.core.dao.SysShortcutMenuMapper;
import com.sc.admin.core.service.SysShortcutMenuService;
import com.sc.common.entity.admin.shortcutmenu.SysShortcutMenu;
import com.sc.common.mapper.IBaseMapper;
import com.sc.common.service.BaseServiceImpl;
import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import com.sc.common.dto.WebResponseDto;

/**
 * @author: wust
 * @date: 2020-02-16 11:25:19
 * @description:
 */
@Service("sysShortcutMenuServiceImpl")
public class SysShortcutMenuServiceImpl extends BaseServiceImpl<SysShortcutMenu> implements SysShortcutMenuService{
    @Autowired
    private SysShortcutMenuMapper sysShortcutMenuMapper;

    @Override
    protected IBaseMapper getBaseMapper() {
        return sysShortcutMenuMapper;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto create(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }


    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto update(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }

    @Transactional(rollbackFor=Exception.class)
    @Override
    public WebResponseDto delete(Object obj) {
        WebResponseDto responseDto = new WebResponseDto();
        return responseDto;
    }
}
