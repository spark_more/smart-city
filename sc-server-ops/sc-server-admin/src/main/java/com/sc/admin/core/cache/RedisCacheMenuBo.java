/**
 * Created by wust on 2020-03-30 10:44:32
 * Copyright © 2020 wust. All rights reserved.
 */
package com.sc.admin.core.cache;


import cn.hutool.core.collection.CollectionUtil;
import com.sc.common.util.cache.SpringRedisTools;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import java.util.List;

/**
 * @author: wust
 * @date: Created in 2020-03-30 10:44:32
 * @description: 菜单缓存
 *
 */
@Component
public class RedisCacheMenuBo {
    @Autowired
    private SpringRedisTools springRedisTools;


    public void init() {

    }


    public void reset() {
    }


    public void add(Object entity){

    }


    public void batchAdd(List<Object> list){
        if(CollectionUtil.isNotEmpty(list)){
            for (Object o : list) {
                add(o);
            }
        }
    }


    public void updateByPrimaryKey(Object primaryKey){

    }


    public void batchUpdate(List<Object> primaryKeys){

    }

    public void deleteByPrimaryKey(Object primaryKey){

    }



    public void batchDelete(List<Object> primaryKeys){

    }
}
