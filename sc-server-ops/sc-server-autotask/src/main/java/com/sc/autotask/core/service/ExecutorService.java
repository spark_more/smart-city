package com.sc.autotask.core.service;


public interface ExecutorService<T> {
    void execute(T t);
}
